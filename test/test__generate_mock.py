import pytest
from testdoublegenerator.generate_mock import determine_namespace, \
                                              find_class_name, \
                                              find_pure_virtual_functions, \
                                              find_typedefs, \
                                              find_structs, \
                                              find_enums, \
                                              find_arguments, \
                                              types_for_templates, \
                                              arguments_for_calling, \
                                              find_prerequisites, \
                                              find_direct_public_ancestors, \
                                              pick_relevant_ancestors

c_code_to_examine = """
    namespace abc {
    namespace def {
    class A;
    }
    namespace ghi
    {
    namespace jkl
    {
    class B;
    namespace mno {
    class C;
    }
    }
    class D BASE_SPECIFIER_LIST {
        // member specification
    };
    }
    }
"""
class_to_mock = "D"


@pytest.mark.parametrize("base_specifier_list", [
    "",
    ": public E",
    ": public ns1::E, public ns2::ns3::F",
])
def it_finds_the_name_of_the_class_to_mock(base_specifier_list):
    assert find_class_name(c_code_to_examine.replace("BASE_SPECIFIER_LIST",
                                                     base_specifier_list)) \
        == class_to_mock


def it_determines_the_namespace_of_the_class_to_mock_correctly():
    namespace_expected = "abc::ghi"

    assert determine_namespace(c_code_to_examine, class_to_mock) \
        == namespace_expected


def it_finds_typedefs():
    cpp_content_to_analyse = r"""
        typedef some_definition  valid_typedef;
        mytypedef other_definition invalid_typedef;
		typedef yet_another_definition another_valid_typedef;
typedef  definition  yet_another_valid_typedef;
    """
    typedefs_expected = ["valid_typedef",
                         "another_valid_typedef",
                         "yet_another_valid_typedef"]

    assert find_typedefs(cpp_content_to_analyse) == typedefs_expected


def it_finds_struct_names():
    cpp_content_to_analyse = r"""
    struct MyStruct {
        int n;
        double d[];
    };
    struct another_struct {};
    """
    struct_names_expected = ["MyStruct", "another_struct"]

    assert find_structs(cpp_content_to_analyse) \
        == struct_names_expected


def it_finds_enum_names():
    cpp_content_to_analyse = r"""
    enum MyEnum { value1, value2 };
    enum another_enum
    {
        value1, value2
    };
    """
    enum_names_expected = ["MyEnum", "another_enum"]

    assert find_enums(cpp_content_to_analyse) \
        == enum_names_expected


@pytest.mark.parametrize("arguments_string, arguments_expected", [
    ("QMap<int,QPair<int,float>>& firstArg,int secondArg, QList<QPair<int, int>> thirdArg",
     ["QMap<int,QPair<int,float>>& firstArg", "int secondArg", "QList<QPair<int, int>> thirdArg"]),
    ("QMap<int,QPair<int,float>>& firstArg",
     ["QMap<int,QPair<int,float>>& firstArg"]),
    ("",
     []),
    ("QMap<int, QPair<int, int>> arg1, QPair<QPair<int,int>, int> arg2",
     ["QMap<int, QPair<int, int>> arg1", "QPair<QPair<int,int>, int> arg2"]),
])
def it_finds_arguments(arguments_string, arguments_expected):
    assert find_arguments(arguments_string) == arguments_expected


@pytest.mark.parametrize("arguments_string, argument_types_expected, argument_names_expected", [
    ("int arg1", "int", "arg1"),
    ("int arg1, float arg2", "int,float", "arg1,arg2"),
    ("int  arg1,float  arg2", "int,float", "arg1,arg2"),
    ("int arg1 = 23, float arg2=42", "int,float", "arg1,arg2"),
])
def it_extracts_argument_types_and_names(arguments_string, argument_types_expected, argument_names_expected):
    assert types_for_templates(arguments_string) == argument_types_expected
    assert arguments_for_calling(arguments_string) == argument_names_expected


def it_knows_which_header_files_to_take_into_account():
    # nb: the following string contains a tab on purpose as Makefiles might contain tabs!
    makefile_to_examine = r"""

target.cpp: ../prerequisite_1.h \
        PREREQUISITE_2.H \
		pre-requisite_3.h
    recipe

"""
    prerequisites_expected = ["../prerequisite_1.h",
                              "PREREQUISITE_2.H",
                              "pre-requisite_3.h"]

    target = "target.cpp"
    assert find_prerequisites(target, makefile_to_examine) \
        == prerequisites_expected


@pytest.mark.parametrize("c_code_to_examine, ancestors_expected", [
    ("class A {", []),
    ("""class ForwardDeclaredClass;
    class A_ : public ns1::ns2::B, public virtual  C,
    virtual public ns3::D,
    virtual protected E, virtual G,
    protected H {""", ["ns1::ns2::B", "C", "ns3::D"])
])
def it_finds_direct_public_ancestors(c_code_to_examine, ancestors_expected):
    assert find_direct_public_ancestors(c_code_to_examine) \
        == ancestors_expected


def it_picks_all_relevant_ancestors():
    ancestors_per_class = {
        "A": {"ancestors": ["ns1::ns2::B", "C"]},
        "ns1::ns2::B": {"ancestors": ["ns3::D", "E"]},
        "C": {"ancestors": []},
        "ns3::D": {"ancestors": ["F"]},
        "E": {"ancestors": []},
        "F": {"ancestors": []},
        "UnreleatedClass": {"ancestors": ["X", "Y"]},
    }
    ancestors_expected = {"ns1::ns2::B", "C", "ns3::D", "E", "F"}
    assert pick_relevant_ancestors(ancestors_per_class, "A") \
        == ancestors_expected


def it_finds_pure_virtual_functions():
    c_code_to_examine = """
    class A {
    virtual void voidFunctionWithoutArguments() = 0;
    virtual void constVoidFunctionWithoutArguments() const = 0;
    virtual int integerFunctionWithFloatArgument(float arg) = 0;
    virtual int integerFunctionWithTwoFloatArguments(float arg1, float arg2) = 0;
    virtual void voidFunctionSpanningTwoLines(int arg1,
                                              int arg2) = 0;
    virtual void constVoidFunctionSpanningMultipleLines(
            int arg1,
            int arg2,
            int arg3
        ) const = 0;
    };
    """
    pure_virtual_functions_expected = [
        ("void", "voidFunctionWithoutArguments", "", " ", ""),
        ("void", "constVoidFunctionWithoutArguments", "", " ", "const"),
        ("int", "integerFunctionWithFloatArgument", "float arg", " ", ""),
        ("int", "integerFunctionWithTwoFloatArguments",
         "float arg1, float arg2", " ", ""),
        ("void", "voidFunctionSpanningTwoLines",
         "int arg1,int arg2", " ", ""),
        ("void", "constVoidFunctionSpanningMultipleLines",
         "int arg1,int arg2,int arg3", " ", "const"),
    ]
    assert find_pure_virtual_functions(c_code_to_examine) \
        == pure_virtual_functions_expected