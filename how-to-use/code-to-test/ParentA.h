#ifndef PARENTA_H
#define PARENTA_H

#include "GrandParent.h"

namespace ns3 {
class ParentA : public ns1::GrandParent {
public:
    virtual int parentClassAMethod(int arg) = 0;
};
}

#endif // PARENTA_H
