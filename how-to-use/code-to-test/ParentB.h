#ifndef PARENTB_H
#define PARENTB_H

namespace ns1 {
namespace ns2 {

class ParentB {
public:
    virtual int parentClassBMethod(int arg) = 0;
};

}
}

#endif // PARENTB_H
