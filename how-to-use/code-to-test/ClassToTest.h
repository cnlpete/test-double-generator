#ifndef CLASSTOTEST_H
#define CLASSTOTEST_H

#include <QObject>
#include <QSharedPointer>

#include "Dependency.h"

namespace ns1 {

class ClassToTest : public QObject {
    Q_OBJECT;

public:
    explicit ClassToTest(QSharedPointer<Dependency> dependency);

    double callDoubleReturningMethodWithIntAndFloatArgument(int intArg, float floatArg);

    void callMethodUsingTypedef(Dependency::MyTypedef arg);
    void callMethodUsingStruct(Dependency::MyStruct arg);
    void callMethodUsingEnum(Dependency::MyEnum arg);

    QPair<int, QPair<int, float>> callMethodWithCommasNotSeparatingArguments(QPair<int, QPair<float, int>> arg1, QPair<QPair<int,int>, float> arg2);

    bool callConstMethod(bool arg);

    int callMethodWithDefaultArgument(int arg);

    QList<int> callOverloadedMethod(int arg1, float arg2);

    QList<int> callMethodsOfAncestors(int arg);

    void passArgumentByReference(QString arg);

signals:
    void signalThatASignalWasReceived(int payloadReceived);

private:
    QSharedPointer<Dependency> dependency;
};

}

#endif // CLASSTOTEST_H
